export class SmtpSettingsDto {
  readonly host: string;
  readonly port: number;
  readonly username: string;
  readonly receiverEmail: string;
  readonly password: string;
}
