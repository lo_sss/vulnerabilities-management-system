import { ProductHistoryComponent } from './../features/products/product-history/product-history.component';
import { ProductDetailsComponent } from './../features/products/product-details/product-details.component';
import { DashboardComponent } from '../features/products/dashboard/dashboard';
import { Routes } from '@angular/router';
import { LoginComponent } from '../components/login/login.component';
import { SearchComponent } from '../features/products/search/search.component';
import { AddProductComponent } from '../features/products/add-product/add-product.component';
import { AdminComponent } from '../features/admin/admin.component';
import { AllProductsComponent } from '../features/products/all-products/all-products.component';
import { NotFoundComponent } from '../components/not-found/not-found.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { AdminGuard } from '../core/guards/admin.guard';

export const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'search-results', canActivate: [AuthGuard], component: SearchComponent },
  { path: 'new-product', canActivate: [AuthGuard], component: AddProductComponent },
  { path: 'dashboard', canActivate: [AuthGuard], component: DashboardComponent },
  {
    path: 'admin',
    canActivate: [AdminGuard],
    loadChildren: () =>
      import('../features/admin/admin.module').then((m) => m.AdminModule)
  },
  { path: 'all-products', canActivate: [AuthGuard], component: AllProductsComponent },
  { path: 'product-details/:id', canActivate: [AuthGuard], component: ProductDetailsComponent },
  { path: 'product-details/:id/history', canActivate: [AuthGuard], component: ProductHistoryComponent },
  { path: 'not-found', component: NotFoundComponent},
  { path: '**', redirectTo: '/not-found' }
];

export const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    pathMatch: 'full',
  },
];
