import { Data } from './product-data';
import { ProductInfoDto } from './product-info-dto';

export class ProductHistoryDTO {
  productInfo: ProductInfoDto;
  data: Data[]
}