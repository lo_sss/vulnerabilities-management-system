import { LoginService } from "./log-in.service";
import { TestBed, async } from "@angular/core/testing";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { of } from "rxjs";

describe("LoginService", () => {
  let httpClient;

  let service: LoginService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    httpClient = {
      get() {},
      post() {},
      put() {},
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [LoginService],
    }).overrideProvider(HttpClient, { useValue: httpClient });

    service = TestBed.get(LoginService);
  }));

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  describe("LoginService", () => {
    it("Should call loginUser once with correct parameters", () => {
      // Arrange
      const username = "username";
      const password = "password";
      const url = `http://localhost:3000/session`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "post").mockReturnValue(returnValue);

      // Act
      const result = service.loginUser(username, password);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, { name: username, password });
    });

    it("Should login user and return correct value", () => {
      // Arrange
      const username = "username";
      const password = "password";
      const url = `http://localhost:3000/session`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "post").mockReturnValue(returnValue);

      // Act
      const result = service.loginUser(username, password);

      // Assert
      result.subscribe((data) => {
        expect(data).toEqual("return value");
      });
    });
  });
});
