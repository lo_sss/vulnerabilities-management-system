import { Product } from './product.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToMany } from "typeorm";

@Entity("cveids")
export class CveId {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ type: "nvarchar", nullable: false })
  public cveId: string;

  @ManyToMany(
    type => Product,
    product => product.cveIds
  )
  public products : Promise<Product[]>;

}
