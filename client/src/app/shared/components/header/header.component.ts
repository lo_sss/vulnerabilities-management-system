import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  username = "Guest"
  isAdmin = false
  constructor(private readonly router: Router) { }

  ngOnInit(): void {
    const user = localStorage.getItem('user')
    if(user){
      this.username = JSON.parse(user).name
      this.isAdmin = JSON.parse(user).role === "Admin"
    }
  }

  logoutClicked(){
    localStorage.clear()
    this.router.navigate(['/'])
  }

}
