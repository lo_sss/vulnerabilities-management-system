import { ResponseMessageDTO } from "./../models/response-message-dto";
import { AuthService } from "./auth.service";
import { Controller, Post, Body, UseGuards, Delete } from "@nestjs/common";
import { Token } from "../common/decorators/token.decorator";
import { AuthGuardWithBlacklisting } from "../common/guards/blacklist.guard";
import { UserLoginDto } from "src/models/user/user-login-dto";

@Controller("session")
export class AuthController {
  public constructor(private readonly authService: AuthService) {}

  @Post()
  public async loginUser(
    @Body() user: UserLoginDto
  ): Promise<{ token: string }> {
    return await this.authService.login(user);
  }

  @Delete()
  @UseGuards(AuthGuardWithBlacklisting)
  public async logoutUser(@Token() token: string): Promise<ResponseMessageDTO> {
    this.authService.blacklistToken(token);

    return {
      msg: "Successful logout"
    };
  }
}
