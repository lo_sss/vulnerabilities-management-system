export class SmtpSettingsDto {
  id: number;
  host: string;
  password: string;
  port: string;
  receiverEmail: string;
  username: string;
  updateDate: string;
}
