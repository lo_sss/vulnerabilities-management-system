import { URL_BASE } from './../../config/config';
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({ providedIn: "root" })
export class LoginService {
  constructor(private readonly http: HttpClient) {}

  loginUser(username: string, password: string) {
    return this.http.post(`${URL_BASE}/session`, {
      name: username,
      password,
    });
  }
}
