import { CveId } from './../../data/entities/cveId.entity';
import { Cpe } from './../../data/entities/cpe.entity';

export class UpdateProductDTO {
  cpeId: number;
  cveIds: string[];
}