import { Product } from './product.entity';
import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  OneToMany,
} from "typeorm";

@Entity("cpes")
export class Cpe {
  @PrimaryGeneratedColumn("increment")
  public id: number;

  @Column({ nullable: false })
  public title: string;

  @Column({ nullable: false })
  public name: string;

  @Column({ nullable: false })
  public type: string;

  @Column({ nullable: false })
  public vendor: string;

  @Column({ nullable: false, length: 100 })
  public product: string;

  @Column({ nullable: false, length: 100 })
  public version: string;

  @OneToMany(
    type => Product,
    product => product.cpe
  )
  public products : Promise<Product[]>;
}
