import { NotificationsService } from './../../../core/services/notifications.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/core/services/product-service';
import { ProductDto } from 'src/app/common/models/product-models/product-dto';

@Component({
  selector: 'app-product-with-cpe',
  templateUrl: './dashboard.html',
  styleUrls: ['./dashboard.css'],
})
export class DashboardComponent implements OnInit {
  allProductsLength: number
  withCveLength: number
  withoutCveLength: number


  constructor(
    private readonly router: Router,
    private readonly productService: ProductService,
    private readonly notificationsService: NotificationsService
  ) {}
  ngOnInit(): void {
    this.productService.getAllProducts().subscribe((data: ProductDto[]) => {
      this.allProductsLength = data.length
      this.withCveLength = data.filter(product => product.cveIds.length).length
      this.withoutCveLength = data.filter(product => !product.cveIds.length).length
    }, 
    (error) => {
      this.notificationsService.error('Can not show dashboard')
    })
  }

  redirectToAllProducts() {
    this.router.navigate(['/all-products']);
  }
}
