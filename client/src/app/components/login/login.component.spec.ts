import {
  HttpClientTestingModule,
  HttpTestingController,
} from "@angular/common/http/testing";
import { SharedService } from "./../../core/services/shared.service";
import { StorageService } from "./../../core/services/storage.service";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { LoginComponent } from "./login.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { MaterialModule } from "../../shared/material/material.module";
import { LoginService } from "../../core/services/log-in.service";
import { of } from "rxjs";
import { Router } from "@angular/router";
import { NotificationsService } from "../../core/services/notifications.service";
import { JwtHelperService, JWT_OPTIONS } from "@auth0/angular-jwt";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

describe("LoginComponent", () => {
  let mockLoginService: any;
  let mockStorage: any;
  let mockSharedService: any;
  let mockRouter: any;
  let mockNotificationService: any;

  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    jest.clearAllMocks();

    mockLoginService = {
      loginUser() {
        // empty
      },
    };

    mockStorage = {
      save() {
        // empty
      },
    };

    mockSharedService = {
      storeUser() {
        // empty
      },
      isUserLoggedIn$() {},
    };

    mockRouter = {
      navigate() {
        // empty
      },
    };

    mockNotificationService = {
      success() {
        // empty
      },
    };

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        BrowserModule,
        MaterialModule,
        HttpClientTestingModule,
      ],
      declarations: [LoginComponent],
      providers: [
        LoginComponent,
        LoginService,
        StorageService,
        SharedService,
        NotificationsService,
        JwtHelperService,
      ],
    })
      .overrideProvider(LoginService, { useValue: mockLoginService })
      .overrideProvider(StorageService, { useValue: mockStorage })
      .overrideProvider(SharedService, { useValue: mockSharedService })
      .overrideProvider(Router, { useValue: mockRouter })
      .overrideProvider(NotificationsService, {
        useValue: mockNotificationService,
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
      });
  }));
  // it("should create", () => {
  //   const component: LoginComponent = TestBed.get(LoginComponent);
  //   expect(component).toBeTruthy();
  // });

  describe("login should", () => {
    it("call loginService.loginUser() once with correect parameters", () => {
      //Arrange
      const mockUsername = "username";
      const mockPassword = "password";

      const spy = jest
        .spyOn(mockLoginService, "loginUser")
        .mockImplementation(() => of(true));

      //Act
      component.login(mockUsername, mockPassword);

      //Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith("username", "password");
    });

    // it("call storage.save() once with correect parameters", () => {
    //   //Arrange
    //   const mockUsername = "username";
    //   const mockPassword = "password";

    //   const spy = jest
    //     .spyOn(mockStorage, "save")
    //     .mockImplementation(() => of(true));

    //   //Act
    //   component.login(mockUsername, mockPassword);

    //   //Assert
    //   expect(spy).toHaveBeenCalledTimes(1);
    //   expect(spy).toHaveBeenCalledWith();
    // });
  });
});
