import { SharedService } from './../services/shared.service';
import { NotificationsService } from './../services/notifications.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly notificationsService: NotificationsService,
    private readonly sharedService: SharedService
  ) {}

  canActivate(): Observable<boolean> {
    return this.sharedService.isUserLoggedIn$.pipe(
      tap((loggedIn) => {
        if (!loggedIn || !localStorage.getItem("token")) {
          this.notificationsService.error(
            `You must be logged in order to see this page!`
          );
          this.router.navigate(['/']);
        }
      })
    );
  }
}
