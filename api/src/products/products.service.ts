import { Data } from './../models/product/product-data';
import { ProductHistoryDTO } from './../models/product/product-history';
import { SavedProductDTO } from './../models/product/product-saved-dto';
import { CreateProductDTO } from './../models/product/product-create-dto';
import { Admin } from 'src/data/entities/admin.entity';
/* eslint-disable @typescript-eslint/no-var-requires */
import { CveId } from 'src/data/entities/cveId.entity';
import { BASE_URL, DEFAULT_SMTP_SETTINGS } from './../constants/api';
import { User } from './../data/entities/user.entity';
import { Product } from 'src/data/entities/product.entity';
import { CustomSystemError } from '../common/exceptions/custom-system.error';
import { ProductHistory } from './../data/entities/product-history.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Cpe } from 'src/data/entities/cpe.entity';
import { Repository, Like } from 'typeorm';
import { ReturnProductDTO } from 'src/models/product/product-return-dto';
import fetch from 'node-fetch';
import { getManager } from 'typeorm';
import { UserDto } from 'src/models/user/user-dto';
import { ProductUpdateBodyDto } from 'src/models/product/product-update-body-dto';
const nodemailer = require('nodemailer');
@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Cpe) private readonly cpeRepo: Repository<Cpe>,
    @InjectRepository(CveId) private readonly cveIdRepo: Repository<CveId>,
    @InjectRepository(Product)
    private readonly productRepo: Repository<Product>,
    @InjectRepository(ProductHistory)
    private readonly productHistoryRepo: Repository<ProductHistory>,
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    @InjectRepository(Admin) private readonly adminRepo: Repository<Admin>,
  ) {}

  arrangeByVendor(results, vendor) {
    let arrangedResults = [];
    results.forEach(cpe => {
      if (cpe.vendor.includes(vendor)) {
        arrangedResults = [cpe, ...arrangedResults];
      } else {
        arrangedResults.push(cpe);
      }
    });
    return arrangedResults;
  }

  arrangeByVersion(results, version) {
    let arrangedResults = [];
    results.forEach(cpe => {
      if (cpe.version === version) {
        arrangedResults = [cpe, ...arrangedResults];
      } else if (cpe.version === '-') {
        const index =
        arrangedResults[0] && arrangedResults[0].version === version ? 1 : 0;
        arrangedResults.splice(index, 0, cpe);
      } else {
        arrangedResults.push(cpe)
      }
    });
    return arrangedResults;
  }

  arrangeResults(results, vendor, version) {
    const arrangedByVendor = this.arrangeByVendor(results, vendor);
    return this.arrangeByVersion(arrangedByVendor, version);
  }

  async findProductsByCveId(cveId) {
    const newMatches = await this.findAllProducts();
    const productsWithCve = [];

    for (const product of newMatches) {
      const productCves = await product.cveIds;
      const includesSearchParam = productCves.find(cve => cve.cveId === cveId);
      if (includesSearchParam) {
        productsWithCve.push(product);
      }
    }

    return productsWithCve;
  }

  async findProductsByCpeName(cpeName) {
    const newMatches = await this.findAllProducts();
    const productsWithCpeName = [];

    for (const product of newMatches) {
      const cpe = await product.cpe;
      if (cpe && cpe.title.includes(cpeName)) {
        productsWithCpeName.push(product);
      }
    }

    return productsWithCpeName;
  }

  async findProductsBySearchParam(searchParam: string, searchBy: string) {
    const newSearchParam = searchParam.replace('*', '%');
    if (searchBy === 'version') {
      return this.productRepo.find({
        where: { version: Like(newSearchParam) },
      });
    }

    if (searchBy === 'cve') {
      return this.findProductsByCveId(searchParam);
    }

    if (searchBy === 'cpe') {
      return await this.findProductsByCpeName(searchParam);
    }

    const entityManager = getManager();
    const matches = await entityManager.query(`SELECT *
    FROM products
    WHERE soundex("${newSearchParam}") = soundex(${searchBy})`);

    if (!matches.length) {
      const newMatches = await this.productRepo.find({
        where: { [searchBy]: Like(`%${newSearchParam}%`) },
      });
      return newMatches;
    }
    return matches;
  }

  async findCpeMatches(name: string, version: string, vendor: string) {
    const entityManager = getManager();
    const matchesByProductName = await entityManager.query(`SELECT *
    FROM cpes
    WHERE (soundex("${name}") = soundex(product) AND soundex("${vendor}") = soundex(vendor))`);

    if (matchesByProductName.length) {
      return this.arrangeResults(matchesByProductName, vendor, version);
    }

    const matchesByVendor = await entityManager.query(`SELECT *
    FROM cpes
    WHERE soundex("${vendor}") = soundex(vendor)`);

    if (!matchesByVendor.length) {
      return await this.productRepo.find({
        where: { name: Like(`%${name}%`) },
      });
    }

    return this.arrangeByVersion(matchesByVendor, version);
  }

  async findCpesByVendor(vendor: string) {
    const matches = await this.cpeRepo.find({ where: { vendor } });
    return matches;
  }

  async findAllProducts(): Promise<ReturnProductDTO[]> {
    const allProducts = await this.productRepo.find({
      where: { isDeleted: false },
    });
    const newProducts = [];
    for (const product of allProducts) {
      const cpe = await product.cpe;
      const cveIds = await product.cveIds;
      const fullProduct = { ...product, cpe, cveIds };
      newProducts.push(fullProduct);
    }
    return newProducts;
  }

  async productHistory(productId: number): Promise<ProductHistoryDTO> {
    const product = await this.productRepo.findOne({
      where: { id: productId },
    });
    const productInfo = {
      id: product.id,
      name: product.name,
      vendor: product.vendor,
      version: product.version,
    };
    const productHistoryEntries = await this.productHistoryRepo.find({
      where: { productId },
    });
    const historyData: Data[] = [];
    for (const entry of productHistoryEntries) {
      historyData.push({
        user: await this.userRepo.findOne({ where: { id: entry.userId } }),
        assignedCpe: await this.findCpeById(entry.assignedCpeId),
        assignedCveIds: entry.assignedCveIds,
        assignedOn: entry.createdOn,
      });
    }

    const productHistory: ProductHistoryDTO = {
      productInfo,
      data: historyData,
    };
    return productHistory;
  }

  async findCpeById(cpeId) {
    return await this.cpeRepo.findOne({ where: { id: cpeId } });
  }

  async findProductById(productId: number): Promise<ReturnProductDTO> {
    const foundProduct: Product = await this.productRepo.findOne({
      where: { id: productId },
    });

    const cpe = await foundProduct.cpe;
    const cveIds = await foundProduct.cveIds;
    const cpes = await this.findCpeMatches(
      foundProduct.name,
      foundProduct.version,
      foundProduct.vendor,
    );

    const returnProduct = { ...foundProduct, cpe, cveIds, cpes };
    return returnProduct;
  }

  async createProduct(product: CreateProductDTO): Promise<SavedProductDTO> {
    const foundProduct: Product = await this.productRepo.findOne({
      where: {
        name: product.name,
        vendor: product.vendor,
        version: product.version,
      },
    });
    if (foundProduct) {
      throw new CustomSystemError(
        'Product with such name already exists!',
        400,
      );
    }

    const productEntity: Product = this.productRepo.create(product);
    const savedProduct: SavedProductDTO = await this.productRepo.save(
      productEntity,
    );
    return savedProduct;
  }

  public async findCveById(cveId: string) {
    const responseCves = await fetch(
      `${BASE_URL}cve/1.0/${cveId}
      `,
    );

    const result = await responseCves.json();
    const cveList = result.result.CVE_Items;
    return cveList[0];
  }

  public async findCvesByCpe(cpeId: number): Promise<Product> {
    const correctCpe = await this.cpeRepo.findOne({
      where: { id: cpeId },
    });
    const cpeMatchString = 'cves/1.0?cpeMatchString=';

    const responseCves = await fetch(
      `${BASE_URL}${cpeMatchString}${correctCpe.name}`,
    );

    const result = await responseCves.json();
    const cveList = result.result.CVE_Items;
    return cveList;
  }

  public async updateProduct(
    productId: number,
    productInfo: ProductUpdateBodyDto,
    user: UserDto,
  ): Promise<SavedProductDTO> {
    const oldProduct: Product = await this.productRepo.findOne({
      where: { id: productId },
    });

    if (oldProduct === undefined || oldProduct.isDeleted) {
      throw new CustomSystemError('No such product found', 404);
    }

    const cpeId = productInfo.productDetails.chosenCpe.id;
    const correctCpe = await this.cpeRepo.findOne({
      where: { id: cpeId },
    });

    const cveIds = productInfo.productDetails.chosenCves;

    const newProduct = this.productRepo.create({ ...oldProduct });
    const cveEntities = [];

    for (const cveId of cveIds) {
      const existingCveId = await this.cveIdRepo.findOne({ cveId });
      if (!existingCveId) {
        const newCveEntity = await this.cveIdRepo.save({ cveId });
        cveEntities.push(newCveEntity);
      } else {
        cveEntities.push(existingCveId);
      }
    }

    newProduct.cpe = Promise.resolve(correctCpe);
    newProduct.cveIds = Promise.resolve(cveEntities);
    const savedProduct = await this.productRepo.save(newProduct);

    const assignedCveIds = cveIds.join(',');
    const assignedCpe = await savedProduct.cpe;
    const newHistory = this.productHistoryRepo.create({
      productId,
      assignedCpeId: assignedCpe.id.toString(),
      assignedCveIds: assignedCveIds,
      userId: user.id,
    });

    await this.productHistoryRepo.save(newHistory);

    this.sendEmail(oldProduct, assignedCpe, assignedCveIds);

    return savedProduct;
  }

  async sendEmail({ name }, { title }, assignedCveIds) {
    const smtpSettings =
      (await this.adminRepo.findOne({ where: { id: 1 } })) ||
      DEFAULT_SMTP_SETTINGS;
    const transport = nodemailer.createTransport({
      host: smtpSettings.host,
      port: smtpSettings.port,
      secure: false,
      auth: {
        user: smtpSettings.username,
        pass: smtpSettings.password,
      },
      debug: false,
      logger: true,
    });

    const message = {
      from: 'admin.management.system.telerik@mail.bg',
      to: smtpSettings.receiverEmail,
      subject: `Product ${name} has been assigned new cpe & cves`,
      text: `Cpe assigned: ${title} / Cve Ids Assigned: ${assignedCveIds}`,
    };
    await transport.sendMail(message, function(err, info) {
      if (err) {
        console.log(err);
      } else {
        console.log(info);
      }
    });
  }
}
