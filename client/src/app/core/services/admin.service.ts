import { URL_BASE } from "../../config/config";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { SmtpSettingsDto } from 'src/app/common/models/smtp-settings-dto';
import { UserDTO } from 'src/app/common/models/user-models/user-dto';

@Injectable({ providedIn: "root" })
export class AdminService {
  constructor(private readonly http: HttpClient) {}

  registerUser(username: string, password: string, email: string): Observable<UserDTO> {
    return this.http.post<UserDTO>(`${URL_BASE}/admin/newUser`, {
      name: username,
      password,
      email
    });
  }

  getSmtpSettings(): Observable<SmtpSettingsDto> {
    return this.http.get<SmtpSettingsDto>(`${URL_BASE}/admin/smtp`);
  }

  resetDefaultSettings(): Observable<SmtpSettingsDto> {
    return this.http.put<SmtpSettingsDto>(`${URL_BASE}/admin/smtp/reset`, null);
  }

  updateSmtpSettings(settings): Observable<SmtpSettingsDto> {
    return this.http.put<SmtpSettingsDto>(`${URL_BASE}/admin/smtp`, settings);
  }

  populateDatabase(){
    return this.http.post(`${URL_BASE}/admin/database`, null)
  }

  updateDatabase(){
    return this.http.post(`${URL_BASE}/admin/update`, null)
  }
}