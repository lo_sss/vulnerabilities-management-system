import { User } from './../../data/entities/user.entity';
import { Cpe } from './../../data/entities/cpe.entity';
import { CveId } from 'src/data/entities/cveId.entity';

export class ReturnProductDTO {
    cpe: Cpe
    cveIds: CveId[];
    cpes: Cpe[];
    id: number;
    name: string;
    vendor: string;
    version: string;
    createdOn: Date;
    updatedOn: Date;
    isDeleted: boolean;
    user: Promise<User>;
}