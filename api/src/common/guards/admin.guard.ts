import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { AuthService } from "src/auth/auth.service";

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private readonly authService: AuthService){

  }
  public canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const user = request.user;

    return user && user.role === "Admin";
  }
}
