import { UserRole } from './../../common/enums/user-role.enum';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { NotificationsService } from '../services/notifications.service';

@Injectable({
    providedIn: 'root'
})
export class AdminGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService,
        private readonly router: Router,
        private readonly notificationsService: NotificationsService
    ) { }

    canActivate(): Observable<boolean> {
        return this.authService.loggedUser$
            .pipe(
                map(() => {
                    const newUser = JSON.parse(localStorage.getItem('user'))
                    if (!newUser || newUser.role !== UserRole.Admin) {
                        this.notificationsService.error("You do not have permission to visit that page.")
                        this.router.navigate(['/']);
                        return false;
                    } else {
                        return true;
                    }
                })
            );
    }
}
