import { Cpe } from './../../data/entities/cpe.entity';
import { User } from './../../data/entities/user.entity';
import { IsNotEmpty } from 'class-validator';

export class Data {
  @IsNotEmpty()
  user: User;

  @IsNotEmpty()
  assignedCpe: Cpe;

  @IsNotEmpty()
  assignedCveIds: string;

  @IsNotEmpty()
  assignedOn: Date
}