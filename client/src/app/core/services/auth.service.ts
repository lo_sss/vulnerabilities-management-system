// import { BaseUserDTO } from './../../common/models/user-models/base-user.dto';
// import { UserDTO } from './../../common/models/user-models/user.dto';
// import { LoginUserDTO } from './../../common/models/user-models/login-user.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, zip } from 'rxjs';
import { tap } from 'rxjs/operators';
import { StorageService } from './storage.service';
import { LoggedUserInfoDTO } from 'src/app/common/models/user-models/logged-user-info-dto';
import { LoginUserDTO } from 'src/app/common/models/user-models/user-login-model';
import { URL_BASE } from 'src/app/config/config';
import { UserDTO } from 'src/app/common/models/user-models/user-dto';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(this.isUserLoggedIn());
  private readonly loggedUserSubject$ = new BehaviorSubject<any>(this.loggedUser());

  private readonly loggedUserInfoSubject$ = new BehaviorSubject<any>(new LoggedUserInfoDTO());

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtService: JwtHelperService,
  ) { }

  get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  get loggedUser$(): Observable<any> {
    return this.loggedUserSubject$.asObservable();
  }

  get loggedUserInfo$(): Observable<Partial<LoggedUserInfoDTO>> {
    return this.loggedUserInfoSubject$.asObservable();
  }

  loggedUserInfoValue$(): LoggedUserInfoDTO {
    return this.loggedUserInfoSubject$.getValue();
  }

  nextLoggedUserInfo$(newSubject: LoggedUserInfoDTO): void {
    this.loggedUserInfoSubject$.next(newSubject);
  }

  login(user: LoginUserDTO): Observable<{ token: string }> {
    return this.http.post<{ token: string }>(`${URL_BASE}/session`, user)
      .pipe(
        tap(({ token }) => {
          try {
            const loggedUser: UserDTO = this.jwtService.decodeToken(token);

            this.storage.save('token', token);
            this.isLoggedInSubject$.next(true);
            this.loggedUserSubject$.next(loggedUser);

          } catch (error) { }
        }),
      );
  }

  logout(): Observable<{ msg: string }> {
    return this.http.delete<{ msg: string }>(`${URL_BASE}/session`)
      .pipe(
        tap(() => {
          try {
            this.storage.clear();
            this.isLoggedInSubject$.next(false);
            this.loggedUserSubject$.next(null);
            this.loggedUserInfoSubject$.next(new LoggedUserInfoDTO());
          } catch (error) { }
        }),
      );
  }

  private isUserLoggedIn(): boolean {
    return !!this.storage.read('token');
  }

  private loggedUser(): UserDTO {
    try {
      const user: UserDTO = this.jwtService.decodeToken(this.storage.read('token'));

      return user;
    } catch (error) {
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }
}
