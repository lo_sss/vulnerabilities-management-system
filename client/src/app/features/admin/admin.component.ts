import { Component } from "@angular/core";
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";
import { AdminService } from 'src/app/core/services/admin.service';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import { SmtpSettingsDto } from 'src/app/common/models/smtp-settings-dto';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.css"],
})
export class AdminComponent {

  settings: SmtpSettingsDto

  constructor(private readonly adminService: AdminService, private readonly notificationService: NotificationsService){}


  ngOnInit(): void {
    this.adminService.getSmtpSettings().subscribe((data: SmtpSettingsDto) => {
      this.settings = data;
    },
    (error) => {
      this.notificationService.error('Can not show SMTP settings');
    });
  }

  populateDatabase(){
    this.adminService.populateDatabase().subscribe((result) => {
      this.notificationService.success("Successfuly populated database.")
    }, (error) => {
      this.notificationService.error('Failed to populate database')
    })
  }

  updateDatabase(){
    this.adminService.updateDatabase().subscribe((result) => {
      this.notificationService.success("Successfuly updated database.")
    }, (error) => {
      this.notificationService.error('Failed to update database')
    })
  }
}
