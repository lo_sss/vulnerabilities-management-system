export class ProductInfoDto {
  id: number;
  name: string;
  vendor: string;
  version: string;
}
