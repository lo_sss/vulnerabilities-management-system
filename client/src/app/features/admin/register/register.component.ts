import { AdminService } from '../../../core/services/admin.service';
import { Component } from "@angular/core";
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";
import { NotificationsService } from 'src/app/core/services/notifications.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent {
  passwordFormControl = new FormControl("", [
    Validators.required,
    Validators.minLength(6),
    Validators.maxLength(32)
  ]);

  usernameFormControl = new FormControl("", [
    Validators.required,
    Validators.minLength(2),
    Validators.maxLength(16),
  ]);

  emailFormControl = new FormControl("", [
    Validators.required,
    Validators.email,
    Validators.maxLength(60),
  ]);

  matcher = new MyErrorStateMatcher();

  constructor(private readonly adminService: AdminService, private readonly notificationsService: NotificationsService) {}

  register(username: string, password: string, email: string): void {
    this.adminService.registerUser(username, password, email).subscribe({
      next: (data) => {
        this.notificationsService.success("Succesfuly created user.")
      },
      error: ({ error }) => {
        this.notificationsService.error("Error creating user!")
      }
    });
  }

}
