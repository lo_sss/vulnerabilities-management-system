import { CpeDto } from '../cpe-cve-models/cpe-dto';
import { UserDTO } from '../user-models/user-dto';

export class ProductHistoryDataDto {
  assignedCpe: CpeDto;
  assignedCveIds: string;
  assignedOn: string;
  user: UserDTO;
  email: string;
  id: number;
  isDeleted: false;
  name: string;
  password: string;
  role: string;
}
