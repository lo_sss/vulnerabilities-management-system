import { SharedService } from './../../core/services/shared.service';
import { StorageService } from './../../core/services/storage.service';
import { LoginService } from './../../core/services/log-in.service';
import { Component } from '@angular/core';
import {
  FormControl,
  Validators,
  FormGroupDirective,
  NgForm,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { UserDTO } from 'src/app/common/models/user-models/user-dto';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  passwordFormControl = new FormControl('', [Validators.required]);

  usernameFormControl = new FormControl('', [Validators.required]);

  matcher = new MyErrorStateMatcher();

  constructor(
    private readonly loginService: LoginService,
    private readonly storage: StorageService,
    private readonly sharedService: SharedService,
    private readonly router: Router,
    private readonly notificationsService: NotificationsService
  ) {}

  login(username: string, password: string): void {
    this.loginService.loginUser(username, password).subscribe(
      (response: { user: UserDTO, token: string }) => {
        this.sharedService.isUserLoggedIn$.next(true);
        this.storage.save('token', response.token);
        this.sharedService.storeUser(response.user);
        this.notificationsService.success(`Welcome back ${response.user.name}`);
        this.router.navigate(['/dashboard']);
      },
      (error) => {
        this.notificationsService.error('Wrong username or password.');
      }
    );
  }
}
