import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity('admin')
export class Admin {
  @PrimaryColumn()
  public id: number;

  @Column({ type: 'nvarchar', nullable: false })
  public host: string;

  @Column({ type: 'nvarchar', nullable: false })
  public port: number;

  @Column({ type: 'nvarchar', nullable: false })
  public username: string;

  @Column({ type: 'nvarchar', nullable: false })
  public password: string;

  @Column({ type: 'nvarchar', nullable: false })
  public receiverEmail: string;

  @Column({ type: "nvarchar", nullable: false})
  public updateDate: Date
}
