export class SavedProductDTO {
name: string;
  vendor: string;
  version: string;
  id: number;
  createdOn: Date;
  updatedOn: Date;
  isDeleted: boolean
}