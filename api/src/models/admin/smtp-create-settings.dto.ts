import { IsNumber, IsString, IsNotEmpty } from 'class-validator';

export class SmtpCreateSettingsDto {
  @IsNotEmpty()
  @IsString()
  readonly host: string;

  @IsNotEmpty()
  @IsNumber()
  readonly port: number;

  @IsNotEmpty()
  @IsString()
  readonly username: string;

  @IsNotEmpty()
  @IsString()
  readonly receiverEmail: string;
  
  @IsNotEmpty()
  @IsString()
  readonly password: string;
}
