import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/core/services/product-service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import { ProductDto } from 'src/app/common/models/product-models/product-dto';
import { CpeDto } from 'src/app/common/models/cpe-cve-models/cpe-dto';
import { CpeCveData } from 'src/app/common/models/cpe-cve-models/cpe-cve-data';
import { CveDto } from 'src/app/common/models/cpe-cve-models/cve-dto';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent implements OnInit {
  public productDetailsData: ProductDto;

  public chosenCpe: CpeDto = null;
  public chosenCves: CveDto[] = null;

  constructor(
    private readonly productService: ProductService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly notificationsService: NotificationsService
  ) {}

  ngOnInit(): void {
    this.fetchProductDetails()
  }

  fetchProductDetails(){
    this.route.params.subscribe(({ id }) => {
      this.productService.getProductDetails(id).subscribe((result: ProductDto) => {
        this.productDetailsData = result;
      });
    }, 
    (error) => {
      this.notificationsService.error('Can not show product details')
    });
  }

  handleDataChange(data: CpeCveData): void {
    this.chosenCpe = data.cpe;
    this.chosenCves = data.cves;
  }

  getCveId(cve: CveDto): number{
    return cve.cve.CVE_data_meta.ID
  }

  onSave(): void {
    if (this.chosenCpe && this.chosenCves) {
      this.productService.saveProductDetails(this.productDetailsData.id, {
        chosenCpe: this.chosenCpe,
        chosenCves: this.chosenCves.map(cve => this.getCveId(cve)),
      }).subscribe(response => {
        this.notificationsService.success("Succesfully saved!")
        this.fetchProductDetails()
      }, (error) => this.notificationsService.error('Can not save changes'))
    }
  }

  showHistory(): void{
    const productId = this.productDetailsData.id
    this.router.navigate([`/product-details/${productId}/history`])
  }
}
