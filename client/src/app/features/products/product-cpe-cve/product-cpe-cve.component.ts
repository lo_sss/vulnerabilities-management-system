import { NotificationsService } from './../../../core/services/notifications.service';
import { ITEMS_PER_PAGE } from './../../../config/config';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductService } from 'src/app/core/services/product-service';
import { CpeDto } from 'src/app/common/models/cpe-cve-models/cpe-dto';
import { CveDto } from 'src/app/common/models/cpe-cve-models/cve-dto';

@Component({
  selector: 'app-product-cpe-cve',
  templateUrl: './product-cpe-cve.component.html',
  styleUrls: ['./product-cpe-cve.component.css'],
})
export class ProductCpeCveComponent implements OnInit {
  @Input()
  productDetails: any

  @Input()
  productId: string;

  @Output()
  public dataChange: EventEmitter<any> = new EventEmitter<any>();

  itemsPerPage = ITEMS_PER_PAGE
  pageCpe = 1
  pageCve = 1

  chosenCpe: CpeDto = null;
  chosenCves = [];
  

  cves: any = null;
  constructor(private readonly productService: ProductService,
    private readonly notificationsService: NotificationsService) {}

  ngOnInit(): void {
    this.chosenCpe = this.productDetails.cpe || null;
    this.chosenCves = this.productDetails.chosenCves || [];

    if (this.chosenCpe) {
      this.onCpeChosen(this.chosenCpe);
    }
  }

  getCveId(cve) {
    return cve.cve.CVE_data_meta.ID;
  }

  getCveDescription(cve) {
    return cve.cve.description.description_data[0].value;
  }

  getCveSeverity(cve){
    return cve.impact.baseMetricV2.severity
  }

  isCveChosen(cve) {
    return this.chosenCves.find(
      (curCve) => this.getCveId(curCve) === this.getCveId(cve)
    );
  }

  sendDataToParent() {
    this.dataChange.next({ cves: this.chosenCves, cpe: this.chosenCpe });
  }

  onCveChosen(cve) {
    if (this.isCveChosen(cve)) {
      this.chosenCves = this.chosenCves.filter(
        (curCve) => this.getCveId(curCve) !== this.getCveId(cve)
      );
    } else {
      this.chosenCves.push(cve);
    }
    this.sendDataToParent();
  }

  sortByCveSeverity(data){
    const high = []
    const medium = []
    const low = []

    data.forEach(cve =>{
      if(this.getCveSeverity(cve) === "HIGH"){
        high.push(cve)
      } else if (this.getCveSeverity(cve) === "MEDIUM"){
        medium.push(cve)
      } else{
        low.push(cve)
      }
    })

    return high.concat(medium, low)

  }

  onCpeChosen(cpe) {
    this.chosenCpe = cpe;
    this.sendDataToParent();
    this.cves = null;
    this.productService
      .getCvesByCpe(this.productId, cpe.id)
      .subscribe((data) => {
        data.sort((x, y) => this.getCveSeverity(x) - this.getCveSeverity(y))
        this.chosenCves = data.filter((cve) =>
          this.productDetails.cveIds.find(
            (cveIdItem) => cveIdItem.cveId === this.getCveId(cve)
          )
        );
        this.cves = this.sortByCveSeverity(data);
      }, 
      (error) => {
        this.notificationsService.error('Can not send data')
      })
  }
}
