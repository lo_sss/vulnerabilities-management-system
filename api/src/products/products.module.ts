import { ProductHistory } from './../data/entities/product-history.entity';
import { Cpe } from 'src/data/entities/cpe.entity';
import { Module } from '@nestjs/common';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from 'src/data/entities/product.entity';
import { User } from 'src/data/entities/user.entity';
import { Admin } from 'src/data/entities/admin.entity';
import { CveId } from 'src/data/entities/cveId.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Product, Cpe, User, CveId, ProductHistory, Admin])],
  controllers: [ProductsController],
  providers: [ProductsService]
})
export class ProductsModule {}
