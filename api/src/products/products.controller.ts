import { ProductHistoryDTO } from './../models/product/product-history';
import { ProductDetailsDTO } from './../models/product/product-details-dto';
import { SavedProductDTO } from './../models/product/product-saved-dto';
import { Product } from './../data/entities/product.entity';
import { UseGuards } from '@nestjs/common';
import { Body, Put } from '@nestjs/common';
import { Post } from '@nestjs/common';
import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Query,
  Param,
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { ReturnProductDTO } from 'src/models/product/product-return-dto';
import { CreateProductDTO } from 'src/models/product/product-create-dto';
import { ProductHistory } from 'src/data/entities/product-history.entity';
import { AuthGuard } from '@nestjs/passport';
import { LoggedUser } from 'src/common/decorators/user.decorator';
import { UserDto } from 'src/models/user/user-dto';
import { ProductUpdateBodyDto } from 'src/models/product/product-update-body-dto';

@Controller('products')
export class ProductsController {
  constructor(private readonly productService: ProductsService) {}

  @Get('/:id/cpe/:cpeId/cves')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async getCvesByCpe(@Param('cpeId') cpeId: string): Promise<Product> {
    return await this.productService.findCvesByCpe(+cpeId);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  public async getAllProducts(): Promise<ReturnProductDTO[]> {
    return await this.productService.findAllProducts();
  }

  @Get('/search/')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  public async getProductsBySearchParam(
    @Query('searchParam') searchParam: string,
    @Query('searchBy') searchBy: string,
  ): Promise<ReturnProductDTO[]> {
    return await this.productService.findProductsBySearchParam(
      searchParam,
      searchBy,
    );
  }

  @Get('/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  public async getProductById(
    @Param('id') productId: string,
  ): Promise<ReturnProductDTO> {
    return await this.productService.findProductById(+productId);
  }

  @Get('/:id/history')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  public async getProductHistory(
    @Param('id') productId: string,
  ): Promise<ProductHistoryDTO> {
    return await this.productService.productHistory(+productId);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'))
  async create(@Body() product: CreateProductDTO): Promise<SavedProductDTO> {
    return await this.productService.createProduct(product);
  }

  @Put('/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  public async updateProduct(
    @Param('id') productId: string,
    @Body() productInfo: ProductUpdateBodyDto,
    @LoggedUser() user: UserDto
  ): Promise<SavedProductDTO> {
    return await this.productService.updateProduct(+productId, productInfo, user);;
  }
}
