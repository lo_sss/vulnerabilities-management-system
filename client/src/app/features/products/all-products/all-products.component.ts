import { ITEMS_PER_PAGE } from './../../../config/config';
import { ProductService } from 'src/app/core/services/product-service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'src/app/core/services/notifications.service';
import { ProductDto } from 'src/app/common/models/product-models/product-dto';

@Component({
  selector: 'app-product-with-cpe',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.css'],
})
export class AllProductsComponent implements OnInit {
  itemsPerPage = ITEMS_PER_PAGE
  pageAllProducts = 1;
  pageWithCve = 1;
  pageWithoutCve = 1

  loading = false;
  allProducts: ProductDto[]
  productsWithCve: ProductDto[];
  productsWithoutCve: ProductDto[];

  WITH_CVE = "withCve"
  WITHOUT_CVE = "withoutCve"
  ALL_PRODUCTS = "allProducts"

  constructor(
    private readonly productService: ProductService,
    private readonly router: Router,
    private readonly notificationsService: NotificationsService
  ) {}

  ngOnInit(): void {
    this.loading = true;

    this.productService.getAllProducts().subscribe((data: ProductDto[]) => {
      this.allProducts = data
      this.productsWithCve = data.filter(product => product.cveIds.length)
      this.productsWithoutCve = data.filter(product => !product.cveIds.length)
    }, 
    (error) => {
      this.notificationsService.error('Can not show all products')
    })
  }

  navigateToProductDetails({ id }): void{
    this.router.navigate(["/product-details/", id]);
  }

  isWithCve(product: ProductDto){
    return product.cveIds.length
  }

  getLabel(mode: string): string{
    const labels = {
      withCve: `${this.productsWithCve.length} Products with Cves`,
      withoutCve: `${this.productsWithoutCve.length} Products without Cves`,
      allProducts: `${this.allProducts.length} Total Products`
    }
    return labels[mode]
  }
}
