import { AdminService } from "./admin.service";
import { TestBed, async } from "@angular/core/testing";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { of } from "rxjs";

describe("ProductService", () => {
  let httpClient;

  let service: AdminService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    httpClient = {
      get() {},
      post() {},
      put() {},
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [AdminService],
    }).overrideProvider(HttpClient, { useValue: httpClient });

    service = TestBed.get(AdminService);
  }));

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  describe("AdminService", () => {
    it("Should call registerUser once and navigate to valid url", () => {
      // Arrange
      const username = "username";
      const password = "password";
      const email = "email";
      const url = `http://localhost:3000/admin/newUser`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "post").mockReturnValue(returnValue);

      // Act
      const result = service.registerUser(username, password, email);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, {
        name: username,
        password,
        email,
      });
    });

    it("Should register user and return valid data", () => {
      // Arrange
      const username = "username";
      const password = "password";
      const email = "email";
      const url = `http://localhost:3000/admin/newUser`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "post").mockReturnValue(returnValue);

      // Act
      const result = service.registerUser(username, password, email);

      // Assert
      result.subscribe((data) => {
        expect(data).toEqual("return value");
      });
    });

    it("Should call getSmtpSettings once with correct parameters", () => {
      // Arrange
      const url = `http://localhost:3000/admin/smtp`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getSmtpSettings();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it("Should get Smtp Settings and return correct value", () => {
      // Arrange

      const url = `http://localhost:3000/admin/smtp`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getSmtpSettings();

      // Assert
      result.subscribe((data) => {
        expect(data).toEqual("return value");
      });
    });

    it("Should call resetDefaultSettings once with correct parameters", () => {
      // Arrange
      const url = `http://localhost:3000/admin/smtp/reset`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "put").mockReturnValue(returnValue);

      // Act
      const result = service.resetDefaultSettings();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, null);
    });

    it("Should reset Default Settings and return correct value", () => {
      // Arrange

      const url = `http://localhost:3000/admin/smtp/reset`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "put").mockReturnValue(returnValue);

      // Act
      const result = service.resetDefaultSettings();

      // Assert
      result.subscribe((data) => {
        expect(data).toEqual("return value");
      });
    });

    it("Should call updateSmtpSettings once with correct parameters", () => {
      // Arrange
      const settings = 1;
      const url = `http://localhost:3000/admin/smtp`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "put").mockReturnValue(returnValue);

      // Act
      const result = service.updateSmtpSettings(settings);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, settings);
    });

    it("Should update Smtp Settings and return correct value", () => {
      // Arrange
      const settings = 1;
      const url = `http://localhost:3000/admin/smtp`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "put").mockReturnValue(returnValue);

      // Act
      const result = service.updateSmtpSettings(settings);

      // Assert
      result.subscribe((data) => {
        expect(data).toEqual("return value");
      });
    });
  });
});
