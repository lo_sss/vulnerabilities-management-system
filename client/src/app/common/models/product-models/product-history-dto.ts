import { ProductInfoDto } from './product-info-dto'
import { ProductHistoryDataDto } from './product-history-data-dto'

export class ProductHistoryDto{
    data: ProductHistoryDataDto[]
    productInfo: ProductInfoDto
}
