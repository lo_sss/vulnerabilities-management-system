import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NotificationsService } from './services/notifications.service';
import { StorageService } from './services/storage.service';
import { AuthService } from './services/auth.service';

@NgModule({
  providers: [
    AuthService,
    NotificationsService,
    StorageService,
  ],
  imports: [
    HttpClientModule,
  ],
  exports: [
    HttpClientModule,
  ]
})
export class CoreModule { }
