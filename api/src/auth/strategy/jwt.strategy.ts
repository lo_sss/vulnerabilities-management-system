import { JWTPayload } from './../../models/payload/jwt-payload';
import { User } from './../../data/entities/user.entity';
import { AuthService } from 'src/auth/auth.service';
import { ConfigService } from "@nestjs/config";
import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
    configService: ConfigService
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get("JWT_SECRET"),
      ignoreExpiration: false
    });
  }

  public async validate(payload: JWTPayload): Promise<User> {
    return await this.authService.findUserByName(payload.name);
  }
}
