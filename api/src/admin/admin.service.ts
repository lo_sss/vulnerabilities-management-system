import { DEFAULT_SMTP_SETTINGS, BASE_URL } from './../constants/api';
import { User } from 'src/data/entities/user.entity';
import { Admin } from 'src/data/entities/admin.entity';
import { CreateUserDTO } from './../models/user/user-create-dto';
/* eslint-disable @typescript-eslint/no-var-requires */
const bigJson = require('big-json');
import { Injectable, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cpe } from 'src/data/entities/cpe.entity';
import fs = require('fs');
import * as bcrypt from 'bcrypt';
import { SmtpSettingsDto } from 'src/models/admin/smtp-settings.dto';
import { UserDto } from 'src/models/user/user-dto';
import fetch from 'node-fetch';

@Injectable()
export class AdminService {
  constructor(
    @InjectRepository(Cpe) private readonly cpeRepo: Repository<Cpe>,
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    @InjectRepository(Admin) private readonly adminRepo: Repository<Admin>,
  ) {}

  getTitle = cpe => {
    const title = cpe.title;
    return Object.entries(title[0])[1][1];
  };

  getName = cpe => {
    return cpe['cpe-23:cpe23-item']['@name'];
  };

  parseName = cpe => {
    const arr = cpe.split(':');

    return {
      type: arr[2],
      vendor: arr[3],
      product: arr[4],
      version: arr[5],
    };
  };

  async populateDatabase() {
    const readStream = fs.createReadStream('cpes.json');
    const parseStream = bigJson.createParseStream();

    await new Promise(res => {
      parseStream.on('data', async pojo => {
        const cpeList = pojo['cpe-list']['cpe-item'];
        let i = 0;

        for (const currentCpe of cpeList) {
          const title = this.getTitle(currentCpe);
          const name = this.getName(currentCpe);
          const { type, vendor, product, version } = this.parseName(name);

          const newCpe = {
            title,
            name,
            type,
            vendor,
            product,
            version,
          } as any;

          if (type === 'a') {
            await this.cpeRepo.save(newCpe);
          }

          if (i === cpeList.length - 1) {
            res();
          }

          i++;
        }
      });
      readStream.pipe(parseStream);
    });
  }

  public async createUser(user: CreateUserDTO): Promise<UserDto> {
    const foundUser: User = await this.userRepo.findOne({
      where: [{ name: user.name }, { email: user.email }],
    });
    if (foundUser) {
      throw new HttpException(
        'User with such username or email already exists!',
        400,
      );
    }

    const userEntity: User = this.userRepo.create(user);
    userEntity.password = await bcrypt.hash(userEntity.password, 10);
    const savedUser = await this.userRepo.save(userEntity);
    return savedUser;
  }

  public async updateCpes() {
    const adminProps = await this.adminRepo.findOne({ where: { id: 1 } });

    if (!adminProps.updateDate) {
      await this.adminRepo.save({
        ...adminProps,
        updateDate: new Date(2020, 6, 1, 0, 0, 0),
      });
    }

    const admin = await this.adminRepo.findOne({ where: { id: 1 } });

    const date =
      admin.updateDate
        .toString()
        .replace(' ', 'T')
        .replace('.', ':') + ' Z';

    const response = await fetch(
      `${BASE_URL}cpes/1.0?resultsPerPage=1000&modStartDate=${date}
      `,
    );

    const parsedResponse = await response.json()

    const cpeList = parsedResponse.result.cpes

    for (const currentCpe of cpeList) {
      const title = currentCpe.titles[0].title;
      const name = currentCpe.cpe23Uri;
      const { type, vendor, product, version } = this.parseName(name);

      const newCpe = {
        title,
        name,
        type,
        vendor,
        product,
        version,
      } as any;

      if (type === 'a') {
        await this.cpeRepo.save(newCpe);
      }
    }

    await this.adminRepo.save({ ...admin, updateDate: new Date()})

    return cpeList;
  }

  async getSmtpSettings(): Promise<SmtpSettingsDto> {
    const smtpSettings: SmtpSettingsDto = await this.adminRepo.findOne({
      where: { id: 1 },
    });

    if (!smtpSettings) {
      return await this.adminRepo.save(DEFAULT_SMTP_SETTINGS);
    }

    return smtpSettings;
  }

  async resetSmtpSettings() {
    return await this.adminRepo.save(DEFAULT_SMTP_SETTINGS);
  }

  async updateSmtpSettings({
    host,
    port,
    username,
    password,
    receiverEmail,
  }): Promise<SmtpSettingsDto> {
    return await this.adminRepo.save({
      id: 1,
      host,
      port,
      username,
      password,
      receiverEmail,
    });
  }
}
