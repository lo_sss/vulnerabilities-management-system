export class CpeDto {
  id: number;
  title: string;
  name: string;
  type: string;
  vendor: string;
  product: string;
  version: string;
}
