import { Injectable } from '@nestjs/common';
import { BASE_URL } from 'src/constants/api';
import fetch from 'node-fetch';


@Injectable()
export class PublicService {
  public async findCveById(cveId: string) {
    const responseCves = await fetch(
      `${BASE_URL}cve/1.0/${cveId}
          `,
    );

    const result = await responseCves.json();
    const cveList = result.result.CVE_Items;
    return cveList[0];
  }
}
