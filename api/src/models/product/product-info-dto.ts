import { IsNotEmpty } from 'class-validator';
export class ProductInfoDto{
    @IsNotEmpty()
    id: number;

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    vendor: string;

    @IsNotEmpty()
    version: string;
    
}