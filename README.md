# Vulnerabilities management system

<!-- ![alt text]() -->

Used Technologies: TypeScript, Node.js, NestJS, TypeORM, MySQL, Angular, Angular Material UI


# Public part - functionalities
* Log in
* Expose the CVEs repository as an API and search by a specific CVE ID

# Private part - functionalities
<!-- ![alt text]() -->

* Number of all products, products with and without CVE assigned
* Show all products, products with and without CVE assigned
* Show product details
* Show best CPE matches of the products - custom algorithm
* Choose and assign one CPE to product
* Show best CVE matches of choosen CPE
* Choose and assign one or more CVEs to product
* Send e-mail to admin when CVE is assigned
* Show product history
* Choose and attach to product one CPE and CVEs in history
* Add new product
* Search product by search option - custom algorithm

# Admin part - functionalities
<!-- ![alt text]() -->

* Configure SMTP settings
* Register user
* Populate database
* Repopulate database
* Update database

# Installation
1. Clone the repo
2. npm install in both frontend and backend
3. run mysql database and make schema cpe_db
4. in backend run npm run start:dev to create entities
5. enter your database options in backend/database/seed
 

 example


    { 
         type: "mysql",
         host: "localhost",
         port: 3306,
         username: "root",
         password: "root",
         database: "cpe_db",
         entities: [join(__dirname, '/../../**/**.entity{.ts,.js}')]
    }

6. npm run seed
7. in frontend ng serve
8. open in browser http://localhost:4200
9. npm run test - for the tests
10. Populate database in admin panel to use all the features
11. Enjoy
