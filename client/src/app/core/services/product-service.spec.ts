import { ProductService } from "./product-service";
import { TestBed, async } from "@angular/core/testing";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { URL_BASE } from "../../config/config";
import { of } from "rxjs";

describe("ProductService", () => {
  let httpClient;

  let service: ProductService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    httpClient = {
      get() {},
      post() {},
      put() {},
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ProductService],
    }).overrideProvider(HttpClient, { useValue: httpClient });

    service = TestBed.get(ProductService);
  }));

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  describe("ProductService", () => {
    it("Should call getProductDetails once to valid url", () => {
      // Arrange
      const productId = 1;
      const url = `${URL_BASE}/products/${productId}`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getProductDetails(productId);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it("Should get all product details with valid data", () => {
      // Arrange
      const productId = 1;
      const url = `${URL_BASE}/products/${productId}`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getProductDetails(productId);

      // Assert
      result.subscribe((data: any) => {
        expect(data).toEqual("return value");
      });
    });
    it("Should call getSearchProductsResults() once to valid url", () => {
      // Arrange
      const searchParam = 1;
      const searchBy = 2;
      const url = `${URL_BASE}/products/search/?searchParam=${searchParam}&searchBy=${searchBy}`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getSearchProductsResults(searchParam, searchBy);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it("Should get search producs results with valid data", () => {
      // Arrange
      const searchParam = 1;
      const searchBy = 2;
      const url = `${URL_BASE}/products/search/?searchParam=${searchParam}&searchBy=${searchBy}`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getSearchProductsResults(searchParam, searchBy);

      // Assert
      result.subscribe((data: any) => {
        expect(data).toEqual("return value");
      });
    });

    it("Should call getAllProducts() once to valid url", () => {
      // Arrange
      const url = `${URL_BASE}/products`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getAllProducts();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it("Should get all producs  with valid data", () => {
      // Arrange

      const url = `${URL_BASE}/products`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getAllProducts();

      // Assert
      result.subscribe((data) => {
        expect(data).toEqual("return value");
      });
    });

    it("Should call getCvesByCpe() once to valid url", () => {
      // Arrange
      const productId = 1;
      const cpeId = 2;
      const url = `${URL_BASE}/products/${productId}/cpe/${cpeId}/cves`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getCvesByCpe(productId, cpeId);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it("Should get Cves by Cpe with valid data", () => {
      // Arrange
      const productId = 1;
      const cpeId = 2;
      const url = `${URL_BASE}/products/${productId}/cpe/${cpeId}/cves`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getCvesByCpe(productId, cpeId);

      // Assert
      result.subscribe((data: any) => {
        expect(data).toEqual("return value");
      });
    });

    it("Should call getProductHistory() once to valid url", () => {
      // Arrange
      const productId = 1;
      const url = `${URL_BASE}/products/${productId}/history`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getProductHistory(productId);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url);
    });

    it("Should get product history with valid data", () => {
      // Arrange
      const productId = 1;
      const url = `${URL_BASE}/products/${productId}/history`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "get").mockReturnValue(returnValue);

      // Act
      const result = service.getProductHistory(productId);

      // Assert
      result.subscribe((data: any) => {
        expect(data).toEqual("return value");
      });
    });

    it("Should call addProduct() once to valid url", () => {
      // Arrange
      const name = "name";
      const vendor = "vendor";
      const version = "version";
      const url = `${URL_BASE}/products`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "post").mockReturnValue(returnValue);

      // Act
      const result = service.addProduct(name, vendor, version);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, { name, vendor, version });
    });

    it("Should get product history with valid data", () => {
      // Arrange
      const name = "name";
      const vendor = "vendor";
      const version = "version";
      const url = `${URL_BASE}/products`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "post").mockReturnValue(returnValue);

      // Act
      const result = service.addProduct(name, vendor, version);

      // Assert
      result.subscribe((data: any) => {
        expect(data).toEqual("return value");
      });
    });

    it("Should call saveProductDetails() once to valid url", () => {
      // Arrange
      const productId = 1;
      const productDetails = "details";
      const url = `${URL_BASE}/products/${productId}`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "put").mockReturnValue(returnValue);

      // Act
      const result = service.saveProductDetails(productId, productDetails);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(url, { productDetails });
    });

    it("Should save product details with valid data", () => {
      // Arrange
      const productId = 1;
      const productDetails = "details";
      const url = `${URL_BASE}/products/${productId}`;
      const returnValue = of("return value");

      const spy = jest.spyOn(httpClient, "put").mockReturnValue(returnValue);

      // Act
      const result = service.saveProductDetails(productId, productDetails);

      // Assert
      result.subscribe((data: any) => {
        expect(data).toEqual("return value");
      });
    });
  });
});
