import { CveId } from 'src/data/entities/cveId.entity';
import { Controller, Get, HttpCode, HttpStatus, Param } from '@nestjs/common';
import { PublicService } from './public.service';

@Controller('public')
export class PublicController {
  constructor(private readonly publicService: PublicService) {}

  @Get('/cves/:id')
  @HttpCode(HttpStatus.OK)
  public async getCveById(@Param('id') id: string): Promise<CveId> {
    return await this.publicService.findCveById(id);
  }
}
