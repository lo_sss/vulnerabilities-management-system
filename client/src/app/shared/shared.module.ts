import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LoaderComponent } from './components/loader/loader.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    LoaderComponent,
    SidebarComponent,
  ],
  imports: [CommonModule, RouterModule, ReactiveFormsModule, NgxPaginationModule, FormsModule, MaterialModule],
  exports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    MaterialModule,
    NgxPaginationModule,
    FooterComponent,
    HeaderComponent,
    LoaderComponent,
    SidebarComponent,
  ],
  //   schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SharedModule {}
