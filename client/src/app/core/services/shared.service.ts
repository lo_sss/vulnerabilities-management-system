import { StorageService } from './storage.service';
import { BehaviorSubject } from "rxjs";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({ providedIn: "root" })
export class SharedService {
  constructor(private readonly storage: StorageService, private readonly client: HttpClient) {}

  public isUserLoggedIn$ = new BehaviorSubject<boolean>(
    !!this.storage.read("token")
  );

  public storeUser(user){
    this.storage.save("user", JSON.stringify(user))
  }

  public getUser(){
    return JSON.parse(localStorage.getItem("user"))
  }

  public isUserAdmin(){
    const user = this.getUser()
    return user ? user.roles.find(role => role.id === 2) : false
  }
}
