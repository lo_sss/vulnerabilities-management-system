import { NotificationsService } from './../../../core/services/notifications.service';
import { ITEMS_PER_PAGE } from './../../../config/config';
import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/core/services/product-service';
import { Router } from '@angular/router';
import { ProductDto } from 'src/app/common/models/product-models/product-dto';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent {
  itemsPerPage = ITEMS_PER_PAGE
  page = 1

  public productsData: ProductDto[];
  
  public searchText: string;
  public searchBy = 'name';
  public searching = false;
  private readonly notificationsService: NotificationsService
  
  constructor(
    private readonly productService: ProductService,
    private readonly router: Router
  ) {}

  navigateToDetailsPage({ id }) {
    this.router.navigate([`product-details/${id}`])
  }

  doSearch() {
    this.searching = true;
    this.productService
      .getSearchProductsResults(this.searchText, this.searchBy)
      .subscribe((data: ProductDto[]) => {
        this.searching = false;
        this.productsData = data;
      }, 
      (error) => {
        this.notificationsService.error('Can not show search results')
      });
  }
}
