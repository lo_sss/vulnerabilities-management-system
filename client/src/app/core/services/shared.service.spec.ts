import { SharedService } from "./shared.service";
import { TestBed, async } from "@angular/core/testing";
import { HttpClientModule } from "@angular/common/http";
import { StorageService } from "./storage.service";
import { of } from "rxjs";

describe("SharedService", () => {
  let storage;

  let service: SharedService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    storage = {
      read() {},
      save() {},
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [SharedService],
    }).overrideProvider(StorageService, { useValue: storage });

    service = TestBed.get(SharedService);
  }));

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  describe("SharedService", () => {
    it("Should call storeUser once with correct parameters", () => {
      // Arrange
      const user = "user";

      const spy = jest.spyOn(storage, "save");

      // Act
      const result = service.storeUser(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(user, '"user"');
    });
  });
});
