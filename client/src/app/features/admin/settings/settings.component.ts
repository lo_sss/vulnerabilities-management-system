import { AdminService } from '../../../core/services/admin.service';
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { SmtpSettingsDto } from 'src/app/common/models/smtp-settings-dto';
import { NotificationsService } from 'src/app/core/services/notifications.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit {
  portFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(1),
  ]);

  usernameFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(2),
    Validators.maxLength(20),
  ]);

  passwordFormControl = new FormControl('', [Validators.required]);

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();

  settings: SmtpSettingsDto;

  constructor(
    private readonly adminService: AdminService,
    private readonly notificationsService: NotificationsService
  ) {}

  ngOnInit(): void {
    this.adminService.getSmtpSettings().subscribe((data: SmtpSettingsDto) => {
      this.settings = data;
    },
    (error) => {
      this.notificationsService.error('Can not show SMTP settings');
    });
  }

  resetDefaultSettings() {
    this.adminService.resetDefaultSettings().subscribe((data) => {
      this.notificationsService.success('Successfuly reseted SMTP settings.');
    },
    (error) => {
      this.notificationsService.error('Can not reset SMTP settings');
    });
  }

  updateSmtpSettings(
    host: string,
    port: string,
    username: string,
    receiverEmail: string,
    password: string
  ) {
    this.adminService
      .updateSmtpSettings({
        host,
        port: +port,
        username,
        receiverEmail,
        password,
      })
      .subscribe(
        (data) => {
          this.notificationsService.success(
            'Successfuly updated Smtp settings.'
          );
        },
        (error) => {
          this.notificationsService.error('Failed to update SMTP settings!');
        }
      );
  }
}
