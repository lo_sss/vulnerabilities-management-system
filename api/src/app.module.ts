import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from "./database/database.module";
import { Module } from "@nestjs/common";
import { ConfigModule } from '@nestjs/config';
import { AdminModule } from './admin/admin.module';
import { ProductsModule } from './products/products.module';
import { CoreModule } from './core/core.module';
import { PublicModule } from './public/public.module';
import Joi = require("@hapi/joi");

@Module({
  imports: [
    AdminModule,
    DatabaseModule,
    CoreModule,
    PublicModule,
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        PORT: Joi.number().default(3000),
        DB_TYPE: Joi.string().required(),
        DB_HOST: Joi.string().required(),
        DB_PORT: Joi.number().required(),
        DB_USERNAME: Joi.string().required(),
        DB_PASSWORD: Joi.string().required(),
        DB_DATABASE_NAME: Joi.string().required()
      }),
    }),
    AuthModule,
    AdminModule,
    ProductsModule
  ],
  controllers: [],
  exports: [AuthModule, ConfigModule]
})
export class AppModule {}
