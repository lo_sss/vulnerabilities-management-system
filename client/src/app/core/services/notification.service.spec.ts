import { TestBed } from "@angular/core/testing";
import { ToastrModule, ToastrService } from "ngx-toastr";
import { NotificationsService } from "./notifications.service";

describe("NotificationsService", () => {
  const toastr = {
    success() {},
    warning() {},
    error() {},
  };

  let service: NotificationsService;

  beforeEach(() => {
    jest.clearAllMocks();

    TestBed.configureTestingModule({
      imports: [ToastrModule],
      providers: [NotificationsService],
    }).overrideProvider(ToastrService, { useValue: toastr });

    service = TestBed.get(NotificationsService);
  });

  it("success should call success", () => {
    const message = "test";
    const spy = jest.spyOn(toastr, "success").mockImplementation(() => {});

    service.success(message);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(message);
  });

  it("warn should call warn", () => {
    const message = "test";
    const spy = jest.spyOn(toastr, "warning").mockImplementation(() => {});

    service.warn(message);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(message);
  });

  it("error should call error", () => {
    const message = "test";
    const spy = jest.spyOn(toastr, "error").mockImplementation(() => {});

    service.error(message);

    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(message);
  });
});
