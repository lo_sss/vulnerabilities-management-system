import { Admin } from 'src/data/entities/admin.entity';
import { User } from './../data/entities/user.entity';
import { CveId } from './../data/entities/cveId.entity';
import { Cpe } from './../data/entities/cpe.entity';
import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from 'src/data/entities/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Product, Cpe, User, CveId, Admin])],
  controllers: [AdminController],
  providers: [AdminService]
})
export class AdminModule {}
