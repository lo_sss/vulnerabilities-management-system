import { CveId } from './cveId.entity';
import { Cpe } from './cpe.entity';
import { User } from "./user.entity";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable
} from "typeorm";

@Entity("products")
export class Product {
  @PrimaryGeneratedColumn("increment")
  public id: number;

  @Column({ type: "nvarchar", nullable: false })
  public name: string;

  @Column({ type: "nvarchar", nullable: false })
  public vendor: string;

  @Column({ type: "nvarchar", nullable: false })
  public version: string;

  @CreateDateColumn()
  public createdOn: Date;

  @UpdateDateColumn()
  public updatedOn: Date;

  @ManyToOne(
  type => Cpe,
  cpe => cpe.products
  )
  public cpe: Promise<Cpe>


  @Column({ type: "boolean", default: false })
  public isDeleted: boolean;

  @ManyToOne(
    type => User,
    user => user.products
  )
  public user: Promise<User>;

  @ManyToMany(
    type => CveId,
    cveId => cveId.products
  )
  @JoinTable()
  public cveIds : Promise<CveId[]>;

}
