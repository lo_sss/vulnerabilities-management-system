import { ProductHistoryDataDto } from './../../../common/models/product-models/product-history-data-dto';
import { ITEMS_PER_PAGE } from './../../../config/config';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from 'src/app/core/services/product-service';
import { ProductHistoryDto } from 'src/app/common/models/product-models/product-history-dto';
import { NotificationsService } from 'src/app/core/services/notifications.service';

@Component({
  selector: 'app-product-history',
  templateUrl: './product-history.component.html',
  styleUrls: ['./product-history.component.css'],
})
export class ProductHistoryComponent implements OnInit {
  itemsPerPage = ITEMS_PER_PAGE
  page = 1
  productHistory: ProductHistoryDto;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly productService: ProductService,
    private readonly notificationsService: NotificationsService
  ) {}

  getCvesNumber(historyEntry: ProductHistoryDataDto): number{
    return historyEntry.assignedCveIds.split(",").length
  }

  ngOnInit(): void {
    this.route.params.subscribe(({ id }) => {
      this.productService.getProductHistory(id).subscribe((productHistory: ProductHistoryDto) => {
        this.productHistory = productHistory;
      });
    }, 
    (error) => {
      this.notificationsService.error('Can not show product history')
    });
  }

  onAttach(historyEntry: ProductHistoryDataDto): void{
    const productId = this.productHistory.productInfo.id
    const productDetails = {
      chosenCpe: historyEntry.assignedCpe,
      chosenCves: historyEntry.assignedCveIds.split(",")
    }
    this.productService.saveProductDetails(productId, productDetails).subscribe(response => {
      this.notificationsService.success('Successfuly attached to Product.')
    }, (error) => {
      this.notificationsService.error('Unable to attach to product!')
    })
  }

  navigateBack(productId: number): void{
    this.router.navigate([`/product-details/${productId}`])
  }
}
