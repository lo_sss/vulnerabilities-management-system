import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/login/login.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { AddProductComponent } from "./features/products/add-product/add-product.component";
import { AllProductsComponent } from "./features/products/all-products/all-products.component";
import { ProductCpeCveComponent } from "./features/products/product-cpe-cve/product-cpe-cve.component";
import { ProductDetailsComponent } from "./features/products/product-details/product-details.component";
import { ProductHistoryComponent } from "./features/products/product-history/product-history.component";
import { DashboardComponent } from "./features/products/dashboard/dashboard";
import { SearchComponent } from "./features/products/search/search.component";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { NgxSmartModalModule } from "ngx-smart-modal";
import { LoaderComponent } from "./shared/components/loader/loader.component";
import { SettingsComponent } from "./features/admin/settings/settings.component";
import { RegisterComponent } from "./features/admin/register/register.component";
import { SidebarComponent } from "./shared/components/sidebar/sidebar.component";
import { FooterComponent } from "./shared/components/footer/footer.component";
import { HeaderComponent } from "./shared/components/header/header.component";
import { TOKEN_INTERCEPTOR } from "./core/interceptors";
import { ErrorComponent } from "./components/error/error.component";
import { ToastrModule } from "ngx-toastr";
import { SharedModule } from "./shared/shared.module";
import { CoreModule } from "./core/core.module";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { JwtModule } from "@auth0/angular-jwt";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ErrorComponent,
    NotFoundComponent,
    AddProductComponent,
    AllProductsComponent,
    ProductCpeCveComponent,
    ProductDetailsComponent,
    ProductHistoryComponent,
    DashboardComponent,
    SearchComponent,
    ErrorComponent,
  ],
  imports: [
    SharedModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule,
    HttpClientModule,
    NgxSmartModalModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: "toast-top-right",
      preventDuplicates: true,
      countDuplicates: true,
    }),
    JwtModule.forRoot({ config: {} }),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [TOKEN_INTERCEPTOR],
  bootstrap: [AppComponent],
})
export class AppModule {}
