// tslint:disable:next-line: variable-name
import { createParamDecorator } from '@nestjs/common';

export const LoggedUser = createParamDecorator((_, req) => req.user);
