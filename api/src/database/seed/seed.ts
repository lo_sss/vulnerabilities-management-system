import { User } from './../../data/entities/user.entity';


import { createConnection, Repository, In } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { join } from 'path';

const seedAdmin = async (connection: any) => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);

  const admin = await userRepo.findOne({
    where: {
      name: 'Admin',
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }


  const name = 'admin';
  const password = 'password123';
  const hashedPassword = await bcrypt.hash(password, 10);

  const newAdmin: User = userRepo.create({
    name,
    password: hashedPassword,
    email: "test@test.com",
    role: "Admin",
  });

  await userRepo.save(newAdmin);
  console.log('Seeded admin successfully!');
};

// Use object as sample below for create connection database options
const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection(
    // Your database options here
  {
  type: "mysql",
  host: "localhost",
  port: 3306,
  username: "lora",
  password: "248600",
  database: "cpe_db",
  entities: [join(__dirname, '/../../**/**.entity{.ts,.js}')]
  }
  );

  await seedAdmin(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
