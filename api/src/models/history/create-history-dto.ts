
export class CreateHistoryDTO {
  public productId: string;
  public userId: number
  public assignedCpeId: number
  public assignedCveIds: string
}

