import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
} from "typeorm";

@Entity("history")
export class ProductHistory {
  @PrimaryGeneratedColumn("increment")
  public id: number;

  @Column({ type: "nvarchar", nullable: false })
  public productId: number;

  @CreateDateColumn()
  public createdOn: Date;

  @Column({type: "nvarchar", nullable: false })
  public userId: number

  @Column({type: "nvarchar", nullable: false })
  public assignedCpeId: string

  @Column({type: "nvarchar", nullable: true})
  public assignedCveIds: string

}
