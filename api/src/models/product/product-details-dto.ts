import { CpeDto } from "../cpe-cve/cpe-dto";
import { IsNotEmpty, IsArray, IsObject } from "class-validator";

export class ProductDetailsDTO{

    @IsObject()
    chosenCpe: CpeDto

    @IsArray()
    chosenCves: string[]
}