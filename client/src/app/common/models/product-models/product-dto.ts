import { CpeDto } from '../cpe-cve-models/cpe-dto'

export class ProductDto{
    cpe: CpeDto
    createdOn: string
    cveIds: string[]
    id: number
    isDeleted: boolean
    name: string
    updatedOn: string
    vendor: string
    version: string
}
