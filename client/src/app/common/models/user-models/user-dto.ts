import { UserRole } from 'src/app/common/enums/user-role.enum';

export class UserDTO {

    id: string;

    name: string;

    role: UserRole;
}
