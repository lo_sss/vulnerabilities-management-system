import { Product } from './product.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
} from "typeorm";

@Entity("users")
export class User {
  @PrimaryGeneratedColumn("increment")
  public id: number;

  @Column({ type: "nvarchar", unique: true, nullable: false, length: 20 })
  public name: string;

  @Column({ type: "nvarchar", unique: true, nullable: false, length: 50 })
  public email: string;

  @Column({ nullable: false })
  public password: string;

  @Column({ type: "nvarchar", default: "Basic" })
  public role: string;

  @Column({ type: "boolean", default: false })
  public isDeleted: boolean;

  @OneToMany(
    type => Product,
    product => product.user
  )
  public products: Promise<Product[]>;

}
