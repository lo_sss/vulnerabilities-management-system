import { Component } from "@angular/core";
import { ErrorStateMatcher } from "@angular/material/core";
import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { ProductService } from "src/app/core/services/product-service";
import { NotificationsService } from 'src/app/core/services/notifications.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: "app-add-product",
  templateUrl: "./add-product.component.html",
  styleUrls: ["./add-product.component.css"],
})
export class AddProductComponent {
  productNameFormControl = new FormControl("", [Validators.required]);
  vendorFormControl = new FormControl("", [Validators.required]);
  versionFormControl = new FormControl("", [Validators.required]);
  matcher = new MyErrorStateMatcher();
  nameText: string = "";
  vendorText: string = "";
  versionText: string = "";

  constructor(
    private readonly router: Router,
    private readonly productService: ProductService,
    private readonly notificationsService: NotificationsService
  ) {}

  onPostSuccess(data) {
    this.router.navigate(["/product-details/", data.id]);
  }

  addProduct(name: string, vendor: string, version: string) {
    if (name !== "" || vendor !== "" || version !== "") {
      this.productService.addProduct(name, vendor, version).subscribe({
        next: (data) => {
          this.notificationsService.success("Product Succesfully Added!")
          this.onPostSuccess(data);
        },
        error: (err) => {
          this.notificationsService.error("Can not create new product.")
        },
      });
    } else {
      this.notificationsService.warn("All fields are required.")
    }
  }
}
