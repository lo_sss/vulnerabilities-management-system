import { IsNotEmpty } from "class-validator";
import { ProductDetailsDTO } from "./product-details-dto";

export class ProductUpdateBodyDto{

    @IsNotEmpty()
    productDetails: ProductDetailsDTO

}