import { ResponseMessageDTO } from './../models/response-message-dto';
import { UseGuards, Get, Put } from '@nestjs/common';
import { Post, HttpCode, HttpStatus, Body } from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { AdminService } from './admin.service';
import { AdminGuard } from 'src/common/guards/admin.guard';
import { AuthGuardWithBlacklisting } from 'src/common/guards/blacklist.guard';
import { AuthGuard } from '@nestjs/passport';
import { SmtpSettingsDto } from 'src/models/admin/smtp-settings.dto';
import { SmtpCreateSettingsDto } from 'src/models/admin/smtp-create-settings.dto';
import { CreateUserDTO } from 'src/models/user/user-create-dto';
import { ReturnUserDTO } from 'src/models/user/user-return-dto';

@Controller('admin')
export class AdminController {
  constructor(private readonly adminService: AdminService) {}

  @Get("/smtp")
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  async getSmtpSettings(): Promise<SmtpSettingsDto> {
    return await this.adminService.getSmtpSettings();
  }

  @Put("/smtp")
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  async updateSmtpSettings(@Body() settings: SmtpCreateSettingsDto): Promise<SmtpSettingsDto> {
    return await this.adminService.updateSmtpSettings(settings);
  }

  @Post('/database')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  async populateCpesInDatabase(): Promise<ResponseMessageDTO> {
    await this.adminService.populateDatabase();
    return { msg: 'Database populated!' };
  }

  @Put("/smtp/reset")
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  async resetSmtpSettings(): Promise<SmtpSettingsDto> {
    return await this.adminService.resetSmtpSettings();
  }

  @Post('/newUser')
  @UseGuards(AuthGuardWithBlacklisting)
  @HttpCode(HttpStatus.CREATED)
  async create(@Body() user: CreateUserDTO): Promise<ReturnUserDTO> {
    return await this.adminService.createUser(user);
  }

  @Post('/update')
  @UseGuards(AuthGuardWithBlacklisting)
  @HttpCode(HttpStatus.CREATED)
  async updateDatabase(): Promise<any> {
    return await this.adminService.updateCpes();
  }
}
