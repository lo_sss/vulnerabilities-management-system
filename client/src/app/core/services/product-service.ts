import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { URL_BASE } from "../../config/config";
import { CveDto } from 'src/app/common/models/cpe-cve-models/cve-dto';
import { ProductDto } from 'src/app/common/models/product-models/product-dto';
import { ProductHistoryDto } from 'src/app/common/models/product-models/product-history-dto';

@Injectable({ providedIn: "root" })
export class ProductService {
  constructor(private readonly http: HttpClient) {}

  getProductDetails(productId): Observable<ProductDto> {
    return this.http.get<ProductDto>(`${URL_BASE}/products/${productId}`);
  }

  getSearchProductsResults(searchParam, searchBy): Observable<ProductDto[]> {
    return this.http.get<ProductDto[]>(
      `${URL_BASE}/products/search/?searchParam=${searchParam}&searchBy=${searchBy}`
    );
  }

  getAllProducts() {
    return this.http.get(`${URL_BASE}/products`);
  }

  addProduct(name: string, vendor: string, version: string): Observable<ProductDto> {
    return this.http.post<ProductDto>(`${URL_BASE}/products`, {
      name,
      vendor,
      version,
    });
  }

  getCvesByCpe(productId, cpeId): Observable<CveDto[]> {
    return this.http.get<CveDto[]>(`${URL_BASE}/products/${productId}/cpe/${cpeId}/cves`);
  }

  saveProductDetails(productId, productDetails): Observable<ProductDto> {
    return this.http.put<ProductDto>(`${URL_BASE}/products/${productId}`, {
      productDetails,
    });
  }

  getProductHistory(productId): Observable<ProductHistoryDto> {
    return this.http.get<ProductHistoryDto>(`${URL_BASE}/products/${productId}/history`);
  }
}
